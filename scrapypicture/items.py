# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib_exp.djangoitem import DjangoItem
from picturl.models import PicturlModel


class PictItem(DjangoItem):
    # define the fields for your item here like:
	django_model = PicturlModel
