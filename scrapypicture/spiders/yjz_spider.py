from scrapy.spider import Spider
from scrapy.selector import Selector
from pict.items import PictItem


class YjzSpider(Spider):
	name = "yjz"
	allowed_domains = ["yjz9.com"]
	start_urls = [
			"http://www.yjz9.com/"
			]
	def parse(self, response):
		#filename = "yjz" 
		#open(filename, 'wb').write(response.body)
		sel = Selector(response)
		items = []
		imgarray = sel.xpath('//*[@id="main_con"]/ul/li/a/img/@src')
		idx = 2
		print "imgarray is len",len(imgarray)
		for img in imgarray:
			item = PictItem()
			#src = img.xpath("//a/img/@src").extract()
			item['url'] = img.extract()
			item['cid'] = 1
			item['id' ] = idx
			items.append(item)	
			idx = idx + 1
		print "item len is :", len(items)
		return items
