# Create your views here.
from serializers import PicturlSerializers

from rest_framework import viewsets
from models import PicturlModel
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)



def picturl(request):
    if request.method == 'GET':
        pict = PicturlModel.objects.all()
        serializer = PicturlSerializers(pict, many=True)
        return JSONResponse(serializer.data)




'''
class PicturlViewSet(viewsets.ModelViewSet):
	queryset = PicturlModel.objects.all()
	serializer_all = PicturlSerializers
'''
