from django.db import models

# Create your models here.


class PicturlModel(models.Model):
	id =  models.IntegerField(primary_key=True)
	url = models.CharField(max_length=1024)
	cid = models.IntegerField()

	class Meta:
		ordering = ('id',)
