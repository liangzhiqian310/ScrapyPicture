from rest_framework import serializers
from models import PicturlModel

class PicturlSerializers(serializers.ModelSerializer):
	class Meta:
		model = PicturlModel
		fields = ('id','url','cid')

