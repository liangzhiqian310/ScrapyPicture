from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from rest_framework.routers import DefaultRouter
from picturl import views

router = DefaultRouter()
#router.register(r'picturl', views.PicturlViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pictcms.views.home', name='home'),
    # url(r'^pictcms/', include('pictcms.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	#url(r'^', include(router.urls)),
	url(r'^picturl/$', views.picturl),
)
